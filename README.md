# unity-prefabs

![CC-BY-SA-4.0](https://i.creativecommons.org/l/by-sa/4.0/80x15.png)

This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/)

This stuff is mainly intended for use in vrchat and there may be some vrchat-specific stuff in here.
Graphics & shader stuff is portable though.
