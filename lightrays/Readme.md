# LightRays

![](https://gitlab.com/vridiot/unity-prefabs/raw/master/lightrays/preview.png)

A simple geometry-based fake light ray setup.

### Instructions

1. Download & import unity package
2. Add Assets/lightshafts/lightshaftsM1.prefab to your scene
3. The material LightRays has 4 tunable paramaters
	1. LightCol: The light colour (rgb)
	2. XSpeed: The speed of the texture drifting across the rays (range[0-1])
	3. YSpeed: The speed of the noise mask moving through the rays (range[0-1])
	4. LightInt: The brightness of the light (float, no limit, don't go crazy please D:)